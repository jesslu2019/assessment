grep -E "Regression: Record Publish: | Type: | wTradePrice | wTradeVolume" opra_example_regression.log > cleaned.log
echo "Cleaned file"

input="./cleaned.log"
in_trade_regression=false

title_line="Record Publish:"

# Loop through all lines of the file
while IFS= read -r line
do
    # line="$( echo "$line" | awk -F '<--' '{print $1}' )"		 # Remove comments starting with "<--" 
    
    # Remove starting "Regression: "
    line="$( echo "$line" | awk -F 'Regression: ' '{print $2}' )"
    
    # Analyze each line, if it starts with "Record Publish:"
    # we know this is beginning of a record
    if [[ $line = *${title_line}* ]]; then
    
        # Assuming next line will be the type of this record
        # Read it
        read -r type_line
        
        # Analyze the type line, if it contains "Type: Trade"
        # we know this is a Trade record
        if [[ $type_line = *"Type: Trade"* ]]; then
        
            # Combine the beginning of the record and Type
            line="$line Type: Trade"
            
            # Print the header of record
            echo "$line"
            
            # Set the flag to true
            # so that going forward we know we are still in a Trade record
            in_trade_regression=true
        else
        
            # If the type line does not contain "Type: Trade"
            # we set the flag to false so that 
            # we know we are not in a Trade record
            in_trade_regression=false
        fi 
    fi
    
    # Use the flag to see if we are in a Trade record
    if "$in_trade_regression"; then
        
        # If current line contains "wTradeVolume" or "wTradePrice"
        # print the line out
        if [[ $line = *wTradeVolume* ]]; then
            echo "$line"
        elif [[ $line = *wTradePrice* ]]; then
            echo "$line"
        fi
    fi
done < "$input"
