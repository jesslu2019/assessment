#!/bin/bash

#1
sed 's/#.*$//g' hosts.real > hosts2.real

#2
grep -Po '..:..:..:..:..:..' hosts2.real | sort | uniq > mac.real

#3
grep -Po '^[0-9]{3}\.[0-9]{3}\.[0-9]\.[0-9]{1,3}' hosts2.real > ip.real
#test for space
sed 's/ /space/g' ip.real